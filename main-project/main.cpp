#include <iostream>
#include <iomanip>

using namespace std;

#include "Doklads.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include "processing.h"




void output(Confirence_program* Confirence)
{
	
	cout << "Время начала доклада: ";
	cout << setw(2) << setfill('0') << Confirence->start.hour << ':';
	cout << setw(2) << setfill('0') << Confirence->start.minutes;
	cout << '\n';
	
	cout << "Дата конца доклада: ";
	cout << setw(2) << setfill('0') << Confirence->finish.hour << ':';
	
	cout << setw(2) << setfill('0') << Confirence->finish.minutes;
	cout << '\n';
	
	cout << "Докладчик: ";
	
	cout << Confirence->lecturer.last_name << " ";
	
	cout << Confirence->lecturer.first_name[0] << ".";
	
	cout << Confirence->lecturer.middle_name[0] << ".";
	cout << '\n';
	
	cout << "Тема доклада: ";
	cout << '"' << Confirence->title << '"';
	cout << '\n';
	cout << '\n';
}
int main()
{
	
	cout << "Laba - 9 - OPI \n";
	cout << "Var - 2 -  Prog_konf\n";
	cout << "Author: Artem Bor\n\n";
	cout << "Gruppa - 14\n";
	Confirence_program* Confirence[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		lect("data.txt", Confirence, size);
		cout << "*****  Программа конференций  *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(Confirence[i]);
		}
		bool (*check_function)(Confirence_program*) = NULL; 

		cout << "\nВыберите способ фильтрации или обработки данных:\n";
		cout << "1)Вывод всех докладов Иванова Ивана Ивановича\n";
		cout << "2)Вывести все доклады длительностью больше 15 минут\n";
		cout << "3)Вывести самый длинный проект _____ 10 laba\n";
		cout << "\nВведите номер : ";
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_Confirence_program_by_author;
			cout << "*****   Вывод всех докладов Иванова Ивана Ивановича   *****\n\n";
			break;
		case 2:
			check_function = check_Confirence_program_by_date;
			cout << "*****  Вывести все доклады длительностью больше 15 минут  *****\n\n";
			break;
		case 3:
		{
			int days = process(Confirence, size);
			int hour = 0, minutes;
			while (days > 60)
			{
				days = days - 60;
				hour++;
			}
			minutes = days;
			cout << "***** Самый длинный проект *****\n\n";
			cout << hour << ":" << minutes << "\n\n";
			break;
		}
		default:
			throw "Некорректный номер пункта";
		}
		
		if (check_function)
		{
			int new_size;
			Confirence_program** filtered = filter(Confirence, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete Confirence[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
